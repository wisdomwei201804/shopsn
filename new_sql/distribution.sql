ALTER TABLE `db_order` ADD COLUMN `distribution_status` TINYINT(1) DEFAULT 0 COMMENT '分销状态,0-未结算订单,1-已结算订单' AFTER `comment_status`;

CREATE TABLE `db_distribution` (
  `id` INT(11) AUTO_INCREMENT,
  `uid` INT(11) NOT NULL COMMENT '被分销者id',
  `p_id` INT(11)  NOT NULL COMMENT '父级id',
  `lv`  INT(11)  NOT NULL COMMENT '分销等级',
  `price` decimal(10,2) NOT NULL COMMENT '得到的金钱',
  `proportion` FLOAT NOT NULL COMMENT '当前数据提成比例',
  `status` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '当前分销记录是否处理,0-未处理,1-已处理',
  `time` INT(10) NOT NULL COMMENT '结算时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 COMMENT='分销记录表';


